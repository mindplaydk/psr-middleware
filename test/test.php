<?php

use Mockery\MockInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Middleware\CallableMiddleware;
use Psr\Middleware\MiddlewareDelegateInterface;

require dirname(__DIR__) . '/vendor/autoload.php';

test(
    'can run CallableMiddleware',
    function () {
        $mw = new CallableMiddleware(function (RequestInterface $request, ResponseInterface $response, MiddlewareDelegateInterface $next) {
            return $next->run($request, $response);
        });

        /** @var MockInterface|RequestInterface $request */
        $request = Mockery::mock(RequestInterface::class);

        /** @var MockInterface|ResponseInterface $response */
        $response = Mockery::mock(ResponseInterface::class);

        /** @var MockInterface|MiddlewareDelegateInterface $next */
        $next = Mockery::mock(MiddlewareDelegateInterface::class);

        $next
            ->shouldReceive('run')
            ->once()
            ->with($request, $response)
            ->andReturn($response);

        $result = $mw->run($request, $response, $next);

        eq($result, $response);
    }
);

exit(run());
