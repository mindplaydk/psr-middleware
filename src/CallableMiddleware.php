<?php

namespace Psr\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CallableMiddleware implements MiddlewareInterface
{
    /**
     * @var callable
     */
    private $middleware;

    /**
     * @param callable $middleware
     */
    public function __construct(callable $middleware)
    {
        $this->middleware = $middleware;
    }

    public function run(
        RequestInterface $request,
        ResponseInterface $response,
        MiddlewareDelegateInterface $delegate
    ) {
        return call_user_func($this->middleware, $request, $response, $delegate);
    }
}
