<?php

namespace Psr\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface MiddlewareInterface
{
    /**
     * @param RequestInterface            $request
     * @param ResponseInterface           $response
     * @param MiddlewareDelegateInterface $delegate
     *
     * @return ResponseInterface
     */
    public function run(
        RequestInterface $request,
        ResponseInterface $response,
        MiddlewareDelegateInterface $delegate
    );
}
