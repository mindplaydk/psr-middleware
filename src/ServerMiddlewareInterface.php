<?php

namespace Psr\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ServerMiddlewareInterface
{
    /**
     * @param ServerRequestInterface      $request
     * @param ResponseInterface           $response
     * @param MiddlewareDelegateInterface $delegate
     *
     * @return ResponseInterface
     */
    public function run(
        ServerRequestInterface $request,
        ResponseInterface $response,
        MiddlewareDelegateInterface $delegate
    );
}
