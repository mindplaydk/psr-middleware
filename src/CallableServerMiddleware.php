<?php

namespace Psr\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CallableServerMiddleware implements ServerMiddlewareInterface
{
    /**
     * @var callable
     */
    private $middleware;

    /**
     * @param callable $middleware
     */
    public function __construct(callable $middleware)
    {
        $this->middleware = $middleware;
    }

    public function run(
        ServerRequestInterface $request,
        ResponseInterface $response,
        MiddlewareDelegateInterface $delegate
    ) {
        return call_user_func($this->middleware, $request, $response, $delegate);
    }
}
