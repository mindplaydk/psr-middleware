<?php

namespace Psr\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface MiddlewareDelegateInterface
{
    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * 
     * @return ResponseInterface
     */
    public function run(
        RequestInterface $request,
        ResponseInterface $response
    );
}
